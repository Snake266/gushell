#ifndef __GUSHELL_H__
#define __GUSHELL_H__

#include <stdint.h>

typedef enum {
    OK_STATUS,
    ERROR_STATUS,
    COMMAND_NOT_FOUND
} ReturnStatus;

int8_t gushell_init();

#endif /* __GUSHELL_H__ */
